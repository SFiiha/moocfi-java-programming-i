
import java.util.Scanner;

public class CountingToHundred {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        int input = Integer.valueOf(scanner.nextLine());
        int i = 0;
        
        for (i = input; i <= 100; i++){
            System.out.println(i);
        }

    }
}
