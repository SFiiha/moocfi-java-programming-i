
import java.util.ArrayList;
import java.util.Scanner;

public class PersonalDetails {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        int age = 0;
        double avg = 0;
        int sum = 0;
        String name = "";
        String longest = "";
        int count = 0;
        
        while (true) {
            String input = scanner.nextLine();
            if (input.equals("")){
                break;
            }
            
            String[] pieces = input.split(",");
            age = Integer.valueOf(pieces[1]);
            sum += age;
            name = pieces[0];
            count++;
            
            if (name.length() > longest.length()) {
                longest = name; 
            }
            
            
        }
        
        System.out.println("Longest name: " + longest);
        System.out.println("Average of the birth: " + (1.0 * sum / count ));


    }
}
