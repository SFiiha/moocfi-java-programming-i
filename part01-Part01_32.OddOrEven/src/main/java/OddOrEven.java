
import java.util.Scanner;

public class OddOrEven {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Give a number:");
        int check = Integer.valueOf(scan.nextLine());
        
        if (check % 2 == 0){
            System.out.println("Number " + check + " is even.");
        } else{
            System.out.println("Number " + check + " is odd.");
        }

        
                
       
    }
}
