
import java.util.Scanner;

public class AverageOfPositiveNumbers {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int sumPositive = 0;
        int countPositive = 0;
        double averagePositive = 0;
        
        while (true){
            System.out.println("Give a number:");
            int input = Integer.valueOf(scanner.nextLine());
            
            if (input == 0){
                break;
            }
            
            if (input > 0){
                sumPositive = sumPositive + input;
                countPositive = countPositive + 1;
                averagePositive = (double) sumPositive / countPositive;
            }
            
        }
        
        if (countPositive == 0){
            System.out.println("Cannot calculate the average");
        } else {
            System.out.println(averagePositive);
        }

    }
}
